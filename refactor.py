import os
import re
from glob import glob
PATH="altunityrunnerfc"
filenames = [y for x in os.walk(PATH) for y in glob(os.path.join(x[0], '*.py'))]

for fn in filenames:
    lines = []
    with open(fn) as f:
        content = f.readlines();
        cn = None
        for line in content:
            if "class" in line:
                cn = re.search("class (.+)\(", line).group(1)
            if "super()" in line:
                nl = re.sub(r'(super\(\))', 'super(' + cn + ', self)', line)
                lines.append(nl)
            else:
                lines.append(line)
                #print line
    with open(fn, 'w') as f:
        for line in lines:
            f.write(line)


