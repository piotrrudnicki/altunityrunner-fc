from setuptools import setup, find_packages

setup(name='altunityrunnerfc',
      version='1.5.2.7',
      description='Alt Unity Runner with Fishing Clash compatibility',
      url='https://gitlab.com/piotrrudnicki/altunityrunner-fc',
      author='AltUnityRunner - Altom. FC Modifications - Piotr Rudnicki',
      author_email='piotr.rudnicki@tensquaregames.com',
      license='opensource',
      packages=find_packages(),
      zip_safe=False)
      
