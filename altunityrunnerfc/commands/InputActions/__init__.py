from altunityrunnerfc.commands.InputActions.move_mouse_and_wait import *
from altunityrunnerfc.commands.InputActions.move_mouse import *
from altunityrunnerfc.commands.InputActions.press_key import *
from altunityrunnerfc.commands.InputActions.press_key_and_wait import *
from altunityrunnerfc.commands.InputActions.scroll_mouse import *
from altunityrunnerfc.commands.InputActions.scroll_mouse_and_wait import *
from altunityrunnerfc.commands.InputActions.swipe import *
from altunityrunnerfc.commands.InputActions.swipe_and_wait import *
from altunityrunnerfc.commands.InputActions.tilt import *
from altunityrunnerfc.commands.InputActions.tap_at_coordinates import *

