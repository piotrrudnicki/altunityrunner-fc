import os
import sys
from time import sleep
import unittest
from appium import webdriver
from altunityrunnerfc import *
from appium.webdriver.common.touch_action import TouchAction

def PATH(p): return os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)

testDoable = True
if testDoable is True:
    print ('Test state doable = ' + str(testDoable))

class SampleAppiumTest(unittest.TestCase):
    result = unittest.TestResult()
    altdriver = None
    platform = "android"

    @classmethod
    def setUpClass (self):
        self.desired_caps = {}
        self.desired_caps['platform'] = 'Android'
        self.desired_caps['deviceName'] = 'device' 
        self.desired_caps['app'] = PATH(r"C:\Users\Pio.Rud\Downloads\FC_autoTests.apk") 
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', self.desired_caps)
        self.altdriver = AltrunUnityDriver(self.driver, self.platform)

    @classmethod
    def tearDownClass(self):
        self.altdriver.stop()
        self.driver.quit()

    def setUp(self):
        self.set_screenshot_dir('%s/screenshots' % (os.getcwd()))
       
    def set_screenshot_dir(self, screenshot_dir): 
        self.screenshot_dir = screenshot_dir
        if not os.path.exists(screenshot_dir):
            os.mkdir(self.screenshot_dir)

    def swipe_to_element_which_contains(self,by,name):
        if(int(self.altdriver.find_object_which_contains(by,name).y) < 900):
            self.altdriver.swipe(300,800,300,1000,0.1)
            self.swipe_to_element_which_contains(by,name)

    def test_game_load(self):
        buttons = self.altdriver.find_objects_which_contains(By.NAME,"pref_")
        button = self.altdriver.find_chui_button_by_name_in_list(buttons,sys.argv[1])
        button.tap()
        self.altdriver.find_object(By.NAME,"authButton").tap()
        authInput = self.altdriver.find_object(By.NAME,"authInput")
        authInput.tap()
        authInput.set_text(sys.argv[2])
        self.altdriver.press_key_and_wait('Return',1,1)
        buttons = self.altdriver.find_objects_which_contains(By.NAME,"green")
        sleep(1)
        self.altdriver.find_chui_button_by_name_in_list(buttons,"play").tap()
        self.altdriver.wait_for_object_which_contains(By.NAME,"florida_inshore")
        
        greenButtons = self.altdriver.find_objects_which_contains(By.NAME,"green")
        self.altdriver.find_chui_button_by_name_in_list(greenButtons,"play").tap()

        self.altdriver.wait_for_object_which_contains(By.NAME,"StarterPackWindow")
        closeButton = self.altdriver.find_object_which_contains(By.NAME, "CloseButton")
        sleep(1)
        closeButton.tap()
        sleep(1)
        self.altdriver.find_object_which_contains(By.NAME, "CastButton-flat-skewed").tap()
        self.altdriver.wait_for_object_which_contains(By.NAME,"MinigameWindow")
        sleep(2)
        pointer = self.altdriver.find_object(By.NAME,"SwipePanel","ChuiCamera")

        self.altdriver.swipe_and_wait(pointer.x,str(int(pointer.y)+75),pointer.x,str(int(pointer.y)+150),0.15)
        elements = self.altdriver.get_all_elements()
        while len([o for o in elements if "Outcome" in o.name]) <= 0:
            self.altdriver.swipe_and_wait(pointer.x,str(int(pointer.y)+150),pointer.x,str(int(pointer.y)-150),0.5)
            sleep(0.5)
            self.altdriver.swipe_and_wait(pointer.x,str(int(pointer.y)-150),pointer.x,str(int(pointer.y)+150),0.5)
            sleep(0.5)
            elements = self.altdriver.get_all_elements()
            if(len([o for o in elements if "SwipePanel" in o.name]) <= 0):
                break

        self.altdriver.wait_for_object_which_contains(By.NAME,"Outcome")
        self.altdriver.wait_for_object(By.NAME,"confirmButton","ChuiCamera",20,2)
        i = 0
        while i < 10:
            i+= 1
            sleep(1)
            print(i)
        self.altdriver.find_object_which_contains(By.NAME,"ConfirmButton").tap()
        
        sleep(2)

        self.altdriver.find_object_which_contains(By.NAME,"CARD-BUTTON").tap()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(SampleAppiumTest)
    result = unittest.TextTestRunner(verbosity=2).run(suite)
    sys.exit(not result.wasSuccessful())
